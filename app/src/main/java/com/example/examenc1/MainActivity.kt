package com.example.examenc1

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var strBanco : TextView
    private lateinit var numCuenta : EditText
    private lateinit var Nombre : EditText
    private lateinit var Banco : EditText
    private lateinit var Saldo : EditText

    private lateinit var btnEnviar : Button
    private lateinit var btnSalir : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEnviar.setOnClickListener { enviar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes(){
        strBanco = findViewById(R.id.txtNombreBanco)
        numCuenta = findViewById(R.id.txtNumeroCuenta)
        Nombre = findViewById(R.id.txtNombreCliente)
        Banco = findViewById(R.id.txtNombreBanco)
        Saldo = findViewById(R.id.txtSaldo)

        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun enviar(){
        //val numCuenta : String
        //val nombre : String
        val banco : String
        //val saldo : Float

        banco = applicationContext.resources.getString(R.string.strBanco)

            val bundle = Bundle()
            bundle.putString("nombre", Nombre.text.toString())
            bundle.putString("banco", Banco.text.toString())
            bundle.putString("saldo", Saldo.text.toString())


            //Hacer intent para llamar otra actividad
            //                              origen              destino         de la peticion
            val intent = Intent(this@MainActivity,CuentaBancoActivity::class.java)
            //enviando el usuario
            intent.putExtras(bundle)

            //Iniciar la actividad - esperando o no respuesta
            startActivity(intent)
    }



    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("EXAMEN C1")
        confirmar.setMessage("¿Desea cerrar la app?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }



}





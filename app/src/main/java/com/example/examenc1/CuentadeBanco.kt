package com.example.examenc1

class CuentadeBanco{
    var numCuenta: Int
    var Nombre: String
    var Banco: String
    var Saldo: Float

    constructor(numCuenta: Int, nombre: String, banco: String, saldo: Float) {
        this.numCuenta = numCuenta
        this.Nombre = nombre
        this.Banco = banco
        this.Saldo = saldo
    }

    fun depositar(depo: Float) {
        this.Saldo = Saldo+depo
    }

    fun retirar(ret: Float): Boolean {
        if (ret <= Saldo) {
            this.Saldo=Saldo-ret
            return true
        }
        else{
            return false
        }
    }

    fun getNumCuenta(): Int? {
        return numCuenta
    }

    fun setNumCuenta(numCuenta: Int) {
        this.numCuenta = numCuenta
    }

    fun getNombre(): String? {
        return Nombre
    }

    fun setNombre(nombre: String) {
        Nombre = nombre
    }

    fun getBanco(): String? {
        return Banco
    }

    fun setBanco(banco: String) {
        Banco = banco
    }

    fun getSaldo(): Float {
        return Saldo
    }

    fun setSaldo(saldo: Float) {
        Saldo = saldo
    }


}
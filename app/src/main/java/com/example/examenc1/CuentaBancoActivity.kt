package com.example.examenc1

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class CuentaBancoActivity : AppCompatActivity() {

private lateinit var lblNombreBanco : TextView
private lateinit var lblNombreCliente : TextView
private lateinit var lblSaldo : TextView
private lateinit var txtCantidad : EditText;
private lateinit var btnDeposito : Button
private lateinit var btnRetiro : Button
private lateinit var btnRegresar : Button

//Declarar el objeto Calculadora
private lateinit var cuentaBanco: CuentadeBanco;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)
        inicializarComponentes()

        // Hacer el paquete
        var bundle:Bundle = intent.extras as Bundle

        var NumeroCuenta:String = bundle.getString("NumeroCuenta").toString()
        var Nombre:String = bundle.getString("Nombre").toString()
        var Banco:String = bundle.getString("Banco").toString()
        var Saldo:String = bundle.getString("Saldo").toString()

        var nombreBanco: String
        nombreBanco = applicationContext.resources.getString(R.string.strBanco)

        // Mostrar información
        this.lblNombreBanco.setText(nombreBanco)
        this.lblNombreCliente.setText(Nombre)
        this.lblSaldo.setText(Saldo)

        // Construir objeto: CuentaBanco
        this.cuentaBanco.numCuenta = NumeroCuenta.toInt()
        this.cuentaBanco.Banco = Banco
        this.cuentaBanco.Nombre = Nombre
        this.cuentaBanco.Saldo = Saldo.toFloat()

        // Eventos clics
        this.btnDeposito.setOnClickListener { this.btnDepositar() }
        this.btnRetiro.setOnClickListener { this.btnRetirar() }
        this.btnRegresar.setOnClickListener { this.btnRegresar() }
    }


    private fun inicializarComponentes(){
        this.lblNombreBanco = findViewById(R.id.lblNombreBanco)
        this.lblNombreCliente = findViewById(R.id.lblNombreCliente)
        this.lblSaldo = findViewById(R.id.lblSaldo)

        this.txtCantidad = findViewById(R.id.txtCantidad)

        this.btnDeposito = findViewById(R.id.btnDeposito)
        this.btnRetiro = findViewById(R.id.btnRetiro)
        this.btnRegresar = findViewById(R.id.btnRegresar)
    }

    private fun btnDepositar(){
        var strCantidad: String = this.txtCantidad.text.toString()

        if(!strCantidad.equals("")){

            this.cuentaBanco.depositar(strCantidad.toFloat())
            this.lblSaldo.setText(this.cuentaBanco.Saldo.toString())
            this.txtCantidad.setText("")

            Toast.makeText(applicationContext, "Deposito Exitoso", Toast.LENGTH_SHORT).show()

        }
        else Toast.makeText(applicationContext, "Ingrese la cantidad", Toast.LENGTH_SHORT).show()
    }


    private fun btnRetirar(){
        var strCantidad: String = this.txtCantidad.text.toString()

        if(!strCantidad.equals("")){

            if(this.cuentaBanco.retirar(strCantidad.toFloat())){
                Toast.makeText(applicationContext, "Retiro Exitoso", Toast.LENGTH_SHORT).show()
                this.lblSaldo.setText(this.cuentaBanco.Saldo.toString())
                this.txtCantidad.setText("")
            }
            else{
                Toast.makeText(applicationContext, "Su saldo es insuficiente", Toast.LENGTH_SHORT).show()
            }

        }
        else Toast.makeText(applicationContext, "Ingrese la cantidad", Toast.LENGTH_SHORT).show()
    }


    private fun btnRegresar(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("EXAMEN C1")
        confirmar.setMessage("¿Desea regresar al MainActivity?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }


}